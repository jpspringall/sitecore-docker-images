[CmdletBinding()]
param(
	[Parameter(Mandatory = $false)]
	[hashtable]$WatchDirectoryParameters
	,
	[Parameter(Mandatory = $false)]
	[string]$IISPort = ""
)

# setup
$ErrorActionPreference = "STOP"

Import-Module WebAdministration

function Delete-Bindings
{
 param(
	[ValidateNotNullOrEmpty()]
	[string]$IISSite
)

Get-WebBinding -Name "$IISSite" | Remove-WebBinding
}

function Add-Bindings
{
 param(
	[ValidateNotNullOrEmpty()]
	[string]$IISSite
	,
	[Parameter(Mandatory = $false)]
	[string]$IISPort = ""
)

New-WebBinding -Name "$IISSite" -Port 80 -IPAddress * -HostHeader * 
if ($IISPort -ne "") {
New-WebBinding -Name "$IISSite" -Port $IISPort -IPAddress * -HostHeader * 
}
}

# print start message
Write-Host ("### Sitecore Docker Development ENTRYPOINT, starting...")

Delete-Bindings -IISSite "Default Web Site"
Add-Bindings -IISSite "Default Web Site" $IISPort


# print start message
Write-Host ("### Sitecore Development Script ENTRYPOINT, starting...")
& "C:\tools\entrypoints\iis\Development.ps1" -WatchDirectoryParameters $WatchDirectoryParameters
Write-Host ("### Sitecore Docker Development ENTRYPOINT, finished...")

